'use strict';

angular.module('dinendash')

.controller('MenuItems', ['$scope', 'database', '$firebase', function ($scope, database, $firebase) {
    $scope.menu_items = $firebase(database.child('menu_items'));
    $scope.categories = $firebase(database.child('categories'));
    $scope.save = function () {
        $scope.menu_items.$save();
    };
    $scope.create_menu = function() {
        $scope.menu_items.$add($scope.new_item);
        $scope.new_item = {};
    };
    $scope.remove_item = function(index){
    	console.log(index);
        //$firebase(database).$child('menu_items').$remove(index);
    };
}]);
