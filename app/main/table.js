'use strict';

angular.module('dinendash')

.controller('Table', ['$scope', 'database', '$firebase', '$routeParams', '$location',
        function ($scope, database, $firebase, $routeParams, $location) {
    var table = $firebase(database.child('tables').child($routeParams.name));	
    table.$bind($scope, 'table');
    $scope.domain = /^.+\/\/.+?(?=\/)/.exec($location.absUrl())[0];
    $scope.clear_table = function(){
    	table.$child('items').$remove();
        table.$child('payments').$remove();
        table.$child('show_qr').$set(false);
    };
    var sum = function (attr, items) {
        if (!items) return 0;
        return _.chain(items)
            .values()
            .pluck(attr)
            .reduce(function(memo, num){ return memo + parseFloat(num || 0, 10); }, 0)
            .value();
    };
    $scope.calculate_tip = _.partial(sum, 'tip');
    $scope.calculate_paid = _.partial(sum, 'subtotal');
    $scope.calculate_total = _.partial(sum, 'price');
}]);
