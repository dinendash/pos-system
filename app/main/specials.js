'use strict';

angular.module('dinendash')

.controller('Specials', ['$scope', 'database', '$firebase', function ($scope, database, $firebase) {
    $scope.menu_items = $firebase(database.child('menu_items'));
}]);
