'use strict';

angular.module('dinendash')

.directive('tableItemsView', function () {
    return {
        templateUrl: '/main/table_item_view.html',
        replace: true,
        scope: {
            table: '=tableItemsView',
        },
        link: function (scope) {
            scope.remove_item =function (item) {
                var key_index = _.values(scope.table.items).indexOf(item);
                scope.table.$child('items').$remove(_.keys(scope.table.items)[key_index]);
            };
            scope.split_item = function (item, split) {
                if (split > 1) {
                    var new_item = _.clone(item);
                    new_item.price = new_item.price / split;
                    new_item.name = new_item.name + " (1/" + split + ")";
                    for (var count = 0; count < split; count++) {
                        scope.table.$child('items').$add(_.clone(new_item));
                    }
                    scope.remove_item(item);
                }
            };
        }
    };
});
