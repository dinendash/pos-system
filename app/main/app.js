'use strict';

angular.module('templates', []);
angular.module('dinendash', ['templates', 'ngRoute', 'firebase', 'dinendash.phone', 'dinendash.base'])

.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/',{
            templateUrl: '/main/splash.html',
            type: 'site',
            title: 'The new way to pay'
        })
        .when('/specials/',{
            controller: 'Specials',
            templateUrl: '/main/specials.html',
            type: 'pos',
            title: 'Specials'
        })
        .when('/tables/', {
            controller: 'Tables',
            templateUrl: '/main/tables.html',
            type: 'pos',
            title: 'Tables'
        })
        .when('/tables/:name/', {
            controller: 'Table',
            templateUrl: '/main/table.html',
            type: 'pos',
            title: 'Table'
        })
        .when('/tables/:name/additem', {
            controller: 'Additem',
            templateUrl: '/main/add_item.html',
            type: 'pos',
            title: 'Add Item'
        })
        .when('/payments/', {
            controller: 'Payments',
            templateUrl: '/main/payments.html',
            type: 'pos',
            title: 'Payments'
        })
        .when('/items/', {
            controller: 'MenuItems',
            templateUrl: '/main/menu_items.html',
            type: 'pos',
            title: 'Items'
        })
        .when('/tables/:name/tabletop', {
            controller: 'TableTop',
            templateUrl: '/main/tabletop.html',
            type: 'tabletop',
            title: 'Tabletop'
        });

    $locationProvider.html5Mode(true);
}])
.controller('App', ['$scope', 'database', '$firebase', function ($scope, database, $firebase) {
    $scope.settings = $firebase(database.child('settings'));
    $scope.loaded = true;
    $scope.button_pressed = function () {
        $scope.$broadcast('button_press');
    };
    $scope.$on('$routeChangeSuccess', function (event, next) {
        var route;

        if (next) {
            route = next.$$route;

            $scope.mobile = route.type === 'mobile';
            $scope.tabletop = route.type === 'tabletop';
            $scope.site = route.type === 'site';
            $scope.pos = route.type === 'pos';

            $scope.title = route.title;
            $scope.bottom_button = route.button;
        }
    });
}]);
