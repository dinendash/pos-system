'use strict';

angular.module('dinendash')

.controller('TableTop', ['$scope', 'database', '$firebase', '$routeParams', '$location', '$document', function ($scope, database, $firebase, $routeParams, $location, $document) {
    var table = $firebase(database.child('tables').child($routeParams.name));

    $scope.menu_items = $firebase(database.child('menu_items'));

    $scope.domain = /^.+\/\/.+?(?=\/)/.exec($location.absUrl())[0];
    table.$bind($scope, 'table');

    var sum = function (attr, items) {
        if (!items) return 0;
        return _.chain(items)
            .values()
            .pluck(attr)
            .reduce(function(memo, num){ return memo + parseFloat(num, 10); }, 0)
            .value();
    };
    $scope.calculate_paid = _.partial(sum, 'subtotal');
    $scope.calculate_total = _.partial(sum, 'price');
    $scope.fullscreen = function () {
        var element = $document[0].documentElement;
        if(element.requestFullscreen) {
            element.requestFullscreen();
        } else if(element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if(element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen();
        } else if(element.msRequestFullscreen) {
            element.msRequestFullscreen();
        }
    }
}]);
