'use strict';

angular.module('dinendash')

.controller('Additem', ['$scope', 'database', '$firebase', '$routeParams', '$location', '$http', '$timeout', function ($scope, database, $firebase, $routeParams, $location, $http, $timeout) {
	var table = $firebase(database.child('tables').child($routeParams.name));
    table.$bind($scope, 'table');

    $scope.categories = $firebase(database.child('categories'));
    $scope.menu_items = $firebase(database.child('menu_items'));
    $scope.selected = {};
    $scope.add_item = function(item) {
        var new_item = _.clone(item);
        if (new_item.special == true) {
            new_item.price = new_item.special_price;
            table.$child('items').$add(new_item);
        }
        else {
        	table.$child('items').$add(item); 
        }    
    };
}]);
