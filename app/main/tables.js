'use strict';

angular.module('dinendash')

.controller('Tables', ['$scope', 'database', '$firebase', function ($scope, database, $firebase) {
    $scope.tables = $firebase(database.child('tables'));
}]);
