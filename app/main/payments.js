'use strict';

angular.module('dinendash')

.controller('Payments', ['$scope', 'database', '$firebase', function ($scope, database, $firebase) {
    $scope.payments = $firebase(database.child('payments'));
}]);
