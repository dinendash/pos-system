'use strict';

angular.module('dinendash.phone')

.service('checkoutService', function () {
    var removes = {},
        payment,
        totals = {};

    return {
        setPayment: function (value) {
            payment = value;
            value.onDisconnect().remove();
        },
        getPayment: function () {
            return payment;
        },
        removeOnDisconnect: function (id, item) {
            removes[id] = item.onDisconnect();
            item.onDisconnect().remove();
        },
        cancel: function (id) {
            removes[id].cancel();
            delete removes[id];
        },
        cancelAll: function () {
            payment.onDisconnect().cancel();
            _.each(removes, function (remove) {
                remove.cancel();
            });
            payment = undefined;
            totals = {};
        }
    };
})