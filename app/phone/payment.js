'use strict';

angular.module('dinendash.phone')
.controller('Payment',
        ['$scope', 'database', '$firebase', '$routeParams', '$location', 'checkoutService', 'totals_calculator', '$http',
        function ($scope, database, $firebase, $routeParams, $location, checkoutService, totals_calculator, $http) {
    var table_id = $routeParams.table_id,
        table = database.child('tables').child(table_id),
        items = table.child('items'),
        payment = checkoutService.getPayment();

    if (!payment) {
        $location.path('/checkout/' + table_id + '/');
        return;
    }

    $firebase(items).$bind($scope, 'items');
    $scope.payment_id = payment.name();
    $scope.calculate_tip = function (percent) {
        return Math.round(($scope.subtotal * percent) * 100) / 100;
    };
    items.on('value', function (snap) {
        var totals = totals_calculator(snap.val(), payment.name())
        $scope.subtotal = totals.subtotal;
        $scope.tax = totals.tax;
    });
    $scope.$on('button_press', function () {
        var handler = StripeCheckout.configure({
            key: 'pk_test_PbcRWGVIazAGH3cRKpBdQiJZ',
            image: '/square.png',
            name: "Mahony & Sons Bill",
            description: "Dine 'N' Dash Checkout",
            currency: 'CAD',
            token: function(token, args) {
                    $scope.processing = true;
                    console.log(Math.floor((subtotal + tip + tax) * 100) / 100);
                    $http.post('/payment/card/' + table_id + '/' + payment.name(), {
                        token: token,
                        tip: tip,
                        tax: tax,
                        subtotal: subtotal,
                        total: Math.floor((subtotal + tip + tax) * 100) / 100,
                    }).success(function () {
                        checkoutService.cancelAll();
                        $location.
                            path('/success/' + payment.name() + '/').
                            search('email', token.email);
                    }).error(function () {
                        $scope.processing = false;
                        $scope.payment_failed = true;
                    });
                }
            }),
            subtotal = $scope.subtotal,
            tip = $scope.tip,
            tax = $scope.tax;

        $scope.payment_failed = false;
        if (!tip) {
            tip = 0;
        }

        tip = Math.floor(tip * 100) / 100;

        handler.open({amount: Math.floor((subtotal + tip + tax) * 100)});
    });
}]);
