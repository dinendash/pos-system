'use strict';

angular.module('dinendash.phone')

.controller('Success', ['$scope', '$routeParams', '$http', '$firebase', 'database', function ($scope, $routeParams, $http, $firebase, database) {
    var payment_id = $routeParams.payment_id,
        payment = database.child('payments').child(payment_id);

    $scope.email = $routeParams.email;
    $firebase(payment).$bind($scope, 'payment');
    $scope.send_email = function (email) {
        $scope.send_failed = false;
        $scope.sending_email = true;
        $http({method: 'POST', url: '/payment/email/' + payment_id, data:{email_address: email}}).
            success(function(data, status, headers, config) {
                $scope.sending_email = false;
                if (data.status == 'sent') {
                    $scope.email_sent = true;
                } else {
                    $scope.send_failed = true;
                }
            }).
            error(function(data, status, headers, config) {
                $scope.sending_email = false;
                $scope.send_failed = true;
            });
    };
}]);
