'use strict';

angular.module('dinendash.phone')

.controller('Checkout', ['$scope', 'database', '$firebase', '$routeParams', '$location', 'checkoutService', 'totals_calculator',
        function ($scope, database, $firebase, $routeParams, $location, checkoutService, totals_calculator) {
    var table = database.child('tables').child($routeParams.table_id),
        items = table.child('items'),
        payments = table.child('payments'),
        payment = checkoutService.getPayment();

    if (payment === undefined) {
        payment = payments.push({});
        checkoutService.setPayment(payment);
    }

    $scope.subtotal = 0;
    $scope.table_id = table.name()
    $scope.payment_id = payment.name();
    $firebase(items).$bind($scope, 'items');
    $scope.payments = $firebase(payments);
    $firebase(payment).$bind($scope, 'payment');
	$scope.set_selected = function (key, item) {
        var totals;
        if (!item.selected) {
            item.selected = $scope.payment_id;
            checkoutService.removeOnDisconnect(key, items.child(key + '/selected'));
        } else if (item.selected == $scope.payment_id) {
            item.selected = false;
            checkoutService.cancel(key);
        }
        totals = totals_calculator($scope.items, $scope.payment_id)
        $scope.subtotal = totals.subtotal;
        $scope.tax = totals.tax;
	};

    $scope.$on('button_press', function () {
        $location.path('/pay/' + $routeParams.table_id + '/');
    });
}]);
