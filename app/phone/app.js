'use strict';

angular.module('dinendash.phone', ['ngRoute', 'firebase', 'dinendash.base'])

.constant('database', new Firebase('https://dinedash.firebaseio.com/'))
.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/checkout/:table_id/',{
            controller: 'Checkout',
            templateUrl: '/phone/checkout.html',
            type: 'mobile',
            button: 'Pay for Items',
            title: 'Checkout'
        })
        .when('/pay/:table_id/',{
            controller: 'Payment',
            templateUrl: '/phone/payment.html',
            type: 'mobile',
            button: 'Continue',
            title: 'Verify'
        })
        .when('/success/:payment_id/',{
            controller: 'Success',
            templateUrl: '/phone/success.html',
            type: 'mobile',
            title: 'Success'
        });

    $locationProvider.html5Mode(true);
}]);
