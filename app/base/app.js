'use strict';

angular.module('dinendash.base', [])

.constant('database', new Firebase('https://dinedash.firebaseio.com/'));
