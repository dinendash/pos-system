'use strict';

angular.module('dinendash.base')

.filter('inCategory', function() {
    return function(items, category) {
        return _.where(items, {category: category});
    };
})
.filter('reverse', function() {
    return function(items) {
        if (_.isObject(items)) {
            items = _.values(items);
        }
        if (_.isArray(items)) {
            return items.reverse();
        }
        return items;
    };
});
