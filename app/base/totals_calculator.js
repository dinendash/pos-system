'use strict';

angular.module('dinendash.base')

.service('totals_calculator', [function () {
    return function (items, payment_id) {
        var items = _.chain(items)
                .values()
                .where({selected: payment_id}),
            subtotal = items
                .pluck('price')
                .reduce(function(memo, num){
                    return memo + parseFloat(num, 10);
                }, 0)
                .value(),
            tax = items
                .reduce(function (memo, item) {
                    var tax_rate = 0.05;
                    return item.price * tax_rate + memo;
                }, 0)
                .value();
        return {
            subtotal: Math.floor(subtotal * 100) / 100,
            tax: Math.floor(tax * 100) / 100,
        };
    };
}]);
