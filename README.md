Resources
=========
[Bootstrap CSS](http://getbootstrap.com/)

[Angular JS Api](http://docs.angularjs.org/api/)

[Angular Firebase Bindings](https://www.firebase.com/docs/angular/reference.html)

[Firebase API](https://www.firebase.com/docs/javascript/firebase/index.html)

Installation
============
# Setup Node.js
```
 sudo add-apt-repository ppa:chris-lea/node.js
 sudo apt-get update
 sudo apt-get install nodejs
```

# Install Dependencies
```
 npm install grunt-cli bower -g
 npm install
 bower install
```

# Run
```
 grunt server
```

# Build
```
 grunt build
```