var glob = require('glob')

module.exports = {
    cdn_files: [
        'https://cdn.firebase.com/v0/firebase.js',
        'https://checkout.stripe.com/checkout.js',
    ],
    library_files: function (prefix) {
        prefix = prefix !== undefined ? prefix : '';
        var files = [
            'underscore/underscore.js',
            'jquery/jquery.js',
            'momentjs/moment.js',
            'angular/angular.js',
            'angular-cookies/angular-cookies.js',
            'angularfire/angularfire.js',
            'angular-route/angular-route.js',
        ];
        if (prefix !== undefined) {
            return files.map(function (file) {
                return prefix + file;
            });
        } else {
            return files;
        }
    },
    application_glob: function (prefix) {
        prefix = prefix !== undefined ? prefix : '';
        return [
            '**/app.js',
            '**/!(app).js',
        ].map(function (glob) {
            return prefix + glob;
        });
    },
    application_files: function (cwd) {
        return this.application_glob()
            .reduce(function (list, pattern) {
                return list.concat(glob.sync(pattern, {cwd: cwd}));
            }, []);
    },
    copy_assets: function (dest) {
        return [
            {expand: true, cwd: 'app/', src: ['**/**.svg', '**/**.gif', '**/**.png'], dest: dest, filter: 'isFile'},
            {expand: true, cwd: 'bower_components/bootstrap/dist/', src: ['fonts/*'], dest: dest, filter: 'isFile'},
        ];
    }
};
